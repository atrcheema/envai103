"""
=================
main
=================
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from easy_mpl import plot
from utils import SAVE
from utils import plot_bars, set_rcParams, get_predictions, regression_plot


set_rcParams()

eff_true, eff_pred, eff_scores = get_predictions("Removal Efficiency",
                                            cv="KFold",
                                            n_splits=5)
qe_true, qe_pred, qe_scores = get_predictions("Adsorption capacity",
                                              cv="KFold",
                                              n_splits=5,
                                              )

# %%

regression_plot(eff_true, eff_pred)

# %%

regression_plot(qe_true, qe_pred, label="Adsorption Capacity mg/g")

# %%

fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(
    2, 3,
    figsize=(12, 7))

# Effect of NaOH conc.

nb2aic_true = np.mean([eff_true[0:7], eff_true[7:14], eff_true[14:21]], axis=0)
nb2ctx5_true = np.mean([eff_true[21:28], eff_true[28:35], eff_true[35:42]], axis=0)
nb2ctx10_true = np.mean([eff_true[42:49], eff_true[49:56], eff_true[56:63]], axis=0)
nb2ctx15_true = np.mean([eff_true[63:70], eff_true[70:77], eff_true[77:84]], axis=0)
nb2ctx20_true = np.mean([eff_true[84:91], eff_true[91:98], eff_true[98:105]], axis=0)

nb2aic_pred = np.mean([eff_true[0:7], eff_true[7:14], eff_true[14:21]], axis=0)
nb2ctx5_pred = np.mean([eff_true[21:28], eff_true[28:35], eff_true[35:42]], axis=0)
nb2ctx10_pred = np.mean([eff_true[42:49], eff_true[49:56], eff_true[56:63]], axis=0)
nb2ctx15_pred = np.mean([eff_true[63:70], eff_true[70:77], eff_true[77:84]], axis=0)
nb2ctx20_pred = np.mean([eff_true[84:91], eff_true[91:98], eff_true[98:105]], axis=0)

xticks = np.array([0, 2, 5, 7, 10, 15, 20])
plot(xticks, nb2aic_true, 'o', color="#034694", label='$Nb_{2}AlC$', ax=ax1, show=False)
plot(xticks, nb2ctx5_true, 'o', color="#165633", label='5$M$ $Nb_{2}CT_{x}$',ax=ax1, show=False)
plot(xticks, nb2ctx10_true, 'o', color="crimson", label='10$M$ $Nb_{2}CT_{x}$',ax=ax1, show=False)
plot(xticks, nb2ctx15_true, 'o', color="teal", label='15$M$ $Nb_{2}CT_{x}$',ax=ax1, show=False)
plot(xticks, nb2ctx20_true, 'o', color="#94177F", label='20$M$ $Nb_{2}CT_{x}$',ax=ax1, show=False)
ax1.legend(handletextpad=0.1)

plot(xticks, nb2aic_pred, '--',  color="#034694",ax=ax1, show=False)
plot(xticks, nb2ctx5_pred, '--',  color="#165633",ax=ax1, show=False)
plot(xticks, nb2ctx10_pred, '--',  color="crimson",ax=ax1, show=False)
plot(xticks, nb2ctx15_pred, '--',  color="teal",ax=ax1, show=False)
ax = plot(xticks, nb2ctx20_pred, '--',  color="#94177F",ax=ax1, show=False)
ax.set_xticks([0,  5,  10, 15, 20])
ax.set_xticklabels([0, 5, 10, 15, 20])
ax.set_xlabel("Time (min)")
ax.set_ylabel("Removal Efficiency (%)")

# Effect of time

time_effect_pred = np.mean([eff_pred[105:116], eff_pred[116:127], eff_pred[127:138]], axis=0)
time_effect_exp = np.mean([eff_true[105:116], eff_true[116:127], eff_true[127:138]], axis=0)
qe_time_effect_pred = np.mean([qe_pred[105:116], qe_pred[116:127], qe_pred[127:138]], axis=0)
qe_time_effect_exp = np.mean([qe_true[105:116], qe_true[116:127], qe_true[127:138]], axis=0)

time_effect = pd.DataFrame(
    np.vstack([time_effect_exp, time_effect_pred]).transpose(),
    columns=['Prediction', 'Exp'])

x_axes = [0, 5, 10, 15, 20, 30, 40, 50, 60, 120, 240]

plot_bars(time_effect_exp, time_effect_pred,
qe_time_effect_exp,
qe_time_effect_exp,
          x_axes,
          xlabel="Time (min)",
          axes=ax2,
          ax2_lower_limit=0,
          ax1_upper_limit=130,
          )


# effect of solution pH
ph_effect_pred = np.mean([eff_pred[186:192], eff_pred[192:198], eff_pred[198:204]], axis=0)
ph_effect_exp = np.mean([eff_true[186:192], eff_true[192:198], eff_true[198:204]], axis=0)
qe_ph_effect_pred = np.mean([qe_pred[186:192], qe_pred[192:198], qe_pred[198:204]], axis=0)
qe_ph_effect_exp = np.mean([qe_true[186:192], qe_true[192:198], qe_true[198:204]], axis=0)

x_axes = [i for i in range(1, 12, 2)]
plot_bars(ph_effect_exp, ph_effect_pred,
          qe_ph_effect_exp,
          qe_ph_effect_pred,
          x_axes,
          xlabel="Solution pH",
          axes=ax3,
          ax2_lower_limit=0,
          ax1_upper_limit=120,
          )

# effect of adsorbent dosage

dosage_effect_pred = np.mean([eff_pred[162:170], eff_pred[170:178], eff_pred[178:186]], axis=0)
dosage_effect_exp = np.mean([eff_true[162:170], eff_true[170:178], eff_true[178:186]], axis=0)
qe_dosage_effect_pred = np.mean([qe_pred[162:170], qe_pred[170:178], qe_pred[178:186]], axis=0)
qe_dosage_effect_exp = np.mean([qe_true[162:170], qe_true[170:178], qe_true[178:186]], axis=0)

x_axes = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 2]
plot_bars(eff_exp=dosage_effect_exp,
          eff_prediction=dosage_effect_pred,
          qe_exp=qe_dosage_effect_exp,
          qe_prediction=qe_dosage_effect_pred,
          x=x_axes,
          xlabel="Adsorbent Dosage (g/L)",
          axes=ax4,
          show=False,
          ax1_upper_limit=160,
          )

# Effect of initial concentration
ci_effect_pred = np.mean([eff_pred[138:146], eff_pred[146:154], eff_pred[154:162]], axis=0)
ci_effect_exp = np.mean([eff_true[138:146], eff_true[146:154], eff_true[154:162]], axis=0)
qe_ci_effect_pred = np.mean([qe_pred[138:146], qe_pred[146:154], qe_pred[154:162]], axis=0)
qe_ci_effect_exp = np.mean([qe_true[138:146], qe_true[146:154], qe_true[154:162]], axis=0)


x_axes = [5, 10, 20, 30, 40, 50, 75, 100]
ax_ = plot_bars(ci_effect_exp, ci_effect_pred,
          qe_ci_effect_exp,
          qe_ci_effect_pred,
          x_axes,
          xlabel="Initial Concentrations",
          axes=ax5,
          )
ax_.set_yticklabels([0, 25, 50, 75, 150, 175, 200])

# Recycling
rec_effect_pred = np.mean([eff_pred[204:209], eff_pred[209:214], eff_pred[214:219]], axis=0)
rec_effect_exp = np.mean([eff_true[204:209], eff_true[209:214], eff_true[214:219]], axis=0)
qe_rec_effect_pred = np.mean([qe_pred[204:209], qe_pred[209:214], qe_pred[214:219]], axis=0)
qe_rec_effect_exp = np.mean([qe_true[204:209], qe_true[209:214], qe_true[214:219]], axis=0)

x_axes = [1, 2, 3, 4, 5]
plot_bars(rec_effect_exp, rec_effect_pred,
          qe_rec_effect_exp,
          qe_rec_effect_pred,
          x_axes,
          xlabel="Cycle Number",
          axes=ax6,
          ax2_lower_limit=0,
          ax1_upper_limit=110,
          add_qe=False,
          add_legend=True,
          leg_pos="upper right"
          )

plt.tight_layout()
if SAVE:
    plt.savefig("results/figures/fig1.png",
                dpi=600,
                bbox_inches="tight")
plt.show()
