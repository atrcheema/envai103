"""
==========================
Exploratory Data Analysis
==========================
"""

import matplotlib.pyplot as plt

from easy_mpl import plot, pie
from easy_mpl.utils import create_subplots

from utils import LABEL_MAP
from utils import read_data

# %%

data, enc = read_data(encoding=None,
                      outputs=['Removal Efficiency',
                               'Adsorption capacity'])

data.shape

# %%

data.head()

# %%
data.tail()

# %%

pie(data['Adsorbent'])

# %%

data_num = data[[col for col in data.columns if col not in ['Adsorbent']]]
fig, axes = create_subplots(data_num.shape[1], figsize=(10, 8))

for ax, col, label  in zip(axes.flat, data_num, data.columns):

    plot(data_num[col].values, ax=ax, ax_kws=dict(ylabel=LABEL_MAP.get(col, col)),
         lw=0.9,
         color='darkcyan', show=False)
plt.tight_layout()
plt.show()

# %%

fig, axes = create_subplots(data_num.shape[1],
                            figsize=(10, 8),
                            sharey="all")

for idx, ax, col, label  in zip(
        range(len(data.columns)),

                                axes.flat,
                                data_num,
                                data.columns):

    ylabel = None
    if idx in [0, 4, 8, 12]:
        ylabel = "Adsorption Capacity"

    plot(data_num[col].values,
    data['Adsorption capacity'],
         '.',
         ax=ax,
         ax_kws=dict(ylabel=ylabel,
                     xlabel=LABEL_MAP.get(col, col)),
         lw=0.9,
         color='darkcyan', show=False)
plt.tight_layout()
plt.show()

# %%

fig, axes = create_subplots(data_num.shape[1],
                            figsize=(10, 8),
                            sharey="all")

for idx, ax, col, label  in zip(
        range(len(data.columns)),

                                axes.flat,
                                data_num,
                                data.columns):

    ylabel = None
    if idx in [0, 4, 8, 12]:
        ylabel = "Removal Efficiency"

    plot(data_num[col].values,
    data['Removal Efficiency'],
         '.',
         ax=ax,
         ax_kws=dict(ylabel=ylabel,
                     xlabel=LABEL_MAP.get(col, col)),
         lw=0.9,
         color='darkcyan', show=False)
plt.tight_layout()
plt.show()
