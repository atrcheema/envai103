"""
====================
Experiments
====================
"""

import matplotlib.pyplot as plt
from ai4water.experiments import MLRegressionExperiments

from utils import read_data
from utils import prepare_data
from utils import print_version_info

# %%

print_version_info()

# %%
# Held Out Validation
# ====================

output = "Removal Efficiency"
TrainX, TestX, TrainY, TestY, _, _ = prepare_data(outputs=output)

inputs = TrainX.columns.tolist()

# %%

comparisons = MLRegressionExperiments()

# %%

comparisons.fit(
    x=TrainX,
    y=TrainY.values,
    include="DTs",
    run_type="dry_run",
)

# %%

comparisons.compare_errors('rmse',
                           cutoff_val=1e6,
                           cutoff_type="less",
                           x=TestX, y=TestY)

# %%
# KFold Cross Validation
# =======================

comparisons = MLRegressionExperiments(
    input_features=inputs,
    output_features= [output],
    train_fraction=1.0,
    show=False
)

# %%

comparisons.fitcv(
    x=TrainX,
    y=TrainY.values,
    validation_data=(TestX, TestY.values),
    include="DTs",
    run_type="dry_run",
    cv="KFold",
    cv_kws={"n_splits": 5}
)

# %%

comparisons.plot_cv_scores(
    exclude=["HistGradientBoostingRegressor",
             "LGBMRegressor", 'AdaBoostRegressor'])
plt.tight_layout()
plt.show()

# %%
# GroupKFold Cross Validation
# ==============================

all_data, _ = read_data(outputs=output)

all_data.shape

# %%

x = all_data.iloc[:, 0:-1]
y = all_data.iloc[:, -1]

# %%
x.shape, y.shape

# %%

comparisons = MLRegressionExperiments(
    input_features=inputs,
    output_features= [output],
    train_fraction=1.0,
    #val_metric=["r2"],
    cross_validator={"GroupKFold": {"n_splits": 5,
                                    'groups': x['Adsorbent'].values.astype(int).reshape(-1,)}},
    show=False,
)

# %%

comparisons.fitcv(
    x=x,
    y=y.values,
    run_type="dry_run",
    scoring=['r2', 'rmse', 'r2_score'],
    include='DTs',
)

# %%

comparisons.plot_cv_scores()
plt.tight_layout()
plt.show()

# %%

comparisons.plot_cv_scores(scoring='r2_score')
plt.tight_layout()
plt.show()