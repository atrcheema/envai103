"""
==================
utils
==================
"""
import warnings

from typing import Union, Any, List, Callable, Tuple

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

from easy_mpl import plot, bar_chart, hist, regplot


from ai4water import Model
from ai4water.utils import TrainTestSplit
from sklearn.model_selection import GroupKFold
from ai4water.utils.utils import get_version_info

from sklearn.model_selection import KFold
from SeqMetrics import RegressionMetrics

from ai4water.utils.utils import METRIC_TYPES

# %%

SAVE = False

LABEL_MAP = {

}

# %%

def _ohe_column(df:pd.DataFrame, col_name:str)->tuple:
    # function for OHE
    assert isinstance(col_name, str)

    # setting sparse to True will return a scipy.sparse.csr.csr_matrix
    # not a numpy array
    encoder = OneHotEncoder(sparse=False)
    ohe_cat = encoder.fit_transform(df[col_name].values.reshape(-1, 1))
    cols_added = [f"{col_name}_{i}" for i in range(ohe_cat.shape[-1])]

    df[cols_added] = ohe_cat

    df.pop(col_name)

    return df, cols_added, encoder

# %%

def le_column(df:pd.DataFrame, col_name)->tuple:
    """label encode a column in dataframe"""
    encoder = LabelEncoder()
    df[col_name] = encoder.fit_transform(df[col_name])
    return df, encoder


def read_data(
        inputs=None,
        outputs="Adsorption capacity",
        encoding="le"
):
    df = pd.read_csv("../data/data.csv")

    cf = df['final conc.']
    ci = df['Initial conc.']
    v = df['Volume (L)']
    m = df['loading (g)']
    qe = ((ci - cf) * v) / m
    qe = qe.fillna(0.0)
    df['Adsorption capacity'] = qe

    df["Removal Efficiency"] = ((ci - cf) / ci) * 100

    default_inputs = ['Adsorbent', 'NaOH conc. (M)', 'Surface area',
                      'Pore volume', 'C (At%)',
                  'Al (At%)', 'Nb (At%)', 'O (At%)', 'Na (At%)', 'Pore size ',
                  'Adsorption time', 'Initial conc.',
                  'loading (g)', 'Solution pH', 'Cycle number']

    if inputs is None:
        inputs = default_inputs

    if outputs is None:
        outputs = ['Adsorption capacity']
    elif not isinstance(outputs, list):
           outputs = [outputs]

    df = df[inputs + outputs]

    ads_encoder = None

    if encoding=="ohe":
        # applying One Hot Encoding
        df, _, ads_encoder = _ohe_column(df, 'Adsorbent')

    elif encoding == "le":
        # applying Label Encoding
        df, ads_encoder = le_column(df, 'Adsorbent')

    return df.dropna(), ads_encoder


def prepare_data(
        inputs=None,
        outputs="Adsorption capacity",
        encoding="le"
):
    data, encoder = read_data(
        inputs=inputs,
        outputs=outputs,
        encoding=encoding
    )

    input_features = data.columns.tolist()[0:-1]
    output_features = data.columns.tolist()[-1:]

    train_x, test_x, train_y, test_y = TrainTestSplit(seed=313).split_by_random(
        data[input_features],
        data[output_features]
    )

    return train_x, test_x, train_y, test_y, data[input_features], data[output_features]

# %%

def plot_bars(eff_exp,
              eff_prediction,
              qe_exp,
              qe_prediction,
              x:list,
              axes=None,
              xlabel="",
              show:bool = False,
              colors = ("salmon", "cadetblue"),
              ax2_lower_limit=-5,
              ax1_upper_limit=200,
              add_qe:bool=True,
              add_legend:bool = False,
              leg_pos = 'upper left'
              ):

    effect = pd.DataFrame(np.vstack([eff_exp, eff_prediction]).transpose(),
                               columns=['Prediction', 'Exp']
                               )

    ax_ = bar_chart(effect,
                   show=False, orient="vertical",
                   color=list(colors),
                   labels=x,
                   ax=axes,
                   )
    ax_.set_xlabel(xlabel)

    ax_.set_ylabel("Removal Efficiency (%)")
    ax_.set_yticklabels(ax_.get_yticklabels())
    ax_.set_ylim(0, ax1_upper_limit)

    if add_legend:
        labels = ['True', 'Predicted']
        handles = [plt.Rectangle((0, 0), 1, 1,
                                 color=colors[idx]) for idx, l in enumerate(labels)]
        ax_.legend(handles, labels, loc=leg_pos)

    if add_qe:
        ax2 = ax_.twinx()
        plot(qe_exp, '--o', ax=ax2, color=colors[0], show=False)
        plot(qe_prediction, '--o', ax=ax2, color=colors[1], show=False)
        ax2.set_ylabel("Adsorption Capacity (mg/g)")
        ax2.set_ylim(bottom=ax2_lower_limit, top=ax2.get_ylim()[1])

    if show:
        plt.show()
    return ax_


def set_rcParams(**kwargs):
    # https://matplotlib.org/stable/tutorials/introductory/customizing.html
    _kwargs = {
        'axes.labelsize': '14',
        'xtick.labelsize': '12',
        'ytick.labelsize': '12',
        'legend.title_fontsize': '12',
        'legend.fontsize': '11',
        'axes.titleweight': 'bold',
        'axes.titlesize': '14',
        'axes.labelweight': 'bold',
        'font.family': 'Times New Roman'
    }

    if kwargs:
        _kwargs.update(kwargs)

    for k,v in _kwargs.items():
        plt.rcParams[k] = v

    return

# %%

def get_predictions(
        output,
        cv = None,
        n_splits=5,
):
    """
    Trains the models for predicting of the specific output
    """
    MODEL = {
        "Removal Efficiency": "ExtraTreesRegressor",
        "Adsorption capacity": "CatBoostRegressor"
    }


    model = Model(model='CatBoostRegressor', verbosity=-1)

    if cv:

        assert cv in ['KFold', 'GroupKFold']
        all_data, _ = read_data(outputs=output)

        x = all_data.iloc[:, 0:-1]
        y = all_data.iloc[:, -1]

        cv_args = {'n_splits': n_splits}
        if cv == "GroupKFold":
            cv_args.update({"groups": x['Adsorbent'].values.astype(int).reshape(-1,)})


        cv_scores, observations, prediction, indices = cross_val_score(
            model, x, y,
            cross_validator=cv,
            cross_validator_args=cv_args,
            scoring=['r2', 'r2_score', 'rmse', 'mae'])

        prediction = np.concatenate(prediction)
        observations = np.concatenate(observations)
        indices = np.concatenate(indices)
        performance = np.array(cv_scores)
        df = pd.DataFrame(
            np.vstack([prediction, observations]).transpose(),
            index=indices, columns=['prediction', 'observation']
        ).sort_index()
        obs = df['observation'].values.reshape(-1,)
        pred = df['prediction'].values.reshape(-1,)
    else:
        TrainX, TestX, TrainY, TestY, _, _ = prepare_data(
            outputs=output)
        performance = model.fit(x=TrainX, y=TrainY.values)

        all_data, _ = read_data(outputs=output)
        obs = all_data.iloc[:, -1].values
        pred = model.predict(all_data.iloc[:, 0:-1])

    return obs, pred, performance

# %%

def residual_plot(
        train_true,
        train_prediction,
        test_true,
        test_prediction,
        label="Prediction",
        train_color="orange",
        test_color="royalblue",
        show:bool = False
):
    fig, axis = plt.subplots(1, 2, sharey="all"
                             , gridspec_kw={'width_ratios': [2, 1]})
    test_y = test_true.reshape(-1, ) - test_prediction.reshape(-1, )
    train_y = train_true.reshape(-1, ) - train_prediction.reshape(-1, )
    train_hist_kws = dict(bins=20, linewidth=0.5,
                          edgecolor="k", grid=False, color=train_color,  # "#009E73"
                          orientation='horizontal')
    hist(train_y, show=False, ax=axis[1],
         label="Training", **train_hist_kws)
    plot(train_prediction, train_y, 'o', show=False,
         ax=axis[0],
         color=train_color,
         markerfacecolor=train_color,
         markeredgecolor="black", markeredgewidth=0.5,
         alpha=0.7, label="Training"
         )

    #****#
    test_hist_kws = dict(bins=40, linewidth=0.5,
                     edgecolor="k", grid=False,
                     color=test_color,
                     orientation='horizontal')
    hist(test_y, show=False, ax=axis[1],
         **test_hist_kws)

    set_xticklabels(axis[1], 3)

    plot(test_prediction, test_y, 'o', show=False,
         ax=axis[0],
         color=test_color,
         markerfacecolor=test_color,
         markeredgecolor="black", markeredgewidth=0.5,
         ax_kws=dict(
             xlabel=label,
             ylabel="Residual",
             legend_kws=dict(loc="upper left"),
         ),
         alpha=0.7, label="Test",
         )
    set_xticklabels(axis[0], 5)
    set_yticklabels(axis[0], 5)
    axis[0].axhline(0.0, color="black")
    plt.subplots_adjust(wspace=0.15)

    if show:
       plt.show()
    return

def set_xticklabels(
        ax:plt.Axes,
        max_ticks:Union[int, Any] = 5,
        dtype = int,
        weight = "bold",
        fontsize:Union[int, float]=12,
        max_xtick_val=None,
        min_xtick_val=None,
        **kwargs
):
    """

    :param ax:
    :param max_ticks:
        maximum number of ticks, if not set, all the default ticks will be used
    :param dtype:
    :param weight:
    :param fontsize:
    :param max_xtick_val:
        maxikum value of tick
    :param min_xtick_val:
    :return:
    """
    return set_ticklabels(ax, "x", max_ticks, dtype, weight, fontsize,
                          max_tick_val=max_xtick_val,
                          min_tick_val=min_xtick_val,
                          **kwargs)


def set_yticklabels(
        ax:plt.Axes,
        max_ticks:Union[int, Any] = 5,
        dtype=int,
        weight="bold",
        fontsize:int=12,
        max_ytick_val = None,
        min_ytick_val = None,
        **kwargs
):
    return set_ticklabels(
        ax, "y", max_ticks, dtype, weight,
        fontsize=fontsize,
        max_tick_val=max_ytick_val,
        min_tick_val=min_ytick_val,
        **kwargs
    )


def set_ticklabels(
        ax:plt.Axes,
        which:str = "x",
        max_ticks:int = 5,
        dtype=int,
        weight="bold",
        fontsize:int=12,
        max_tick_val = None,
        min_tick_val = None,
        **kwargs
):
    """

    :param ax:
    :param which:
    :param max_ticks:
    :param dtype:
    :param weight:
    :param fontsize:
    :param max_tick_val:
    :param min_tick_val:
    :param kwargs:
        any keyword arguments of axes.set_{x/y}ticklabels()
    :return:
    """
    ticks_ = getattr(ax, f"get_{which}ticks")()
    ticks = np.array(ticks_)
    if len(ticks)<1:
        warnings.warn(f"can not get {which}ticks {ticks_}")
        return

    if max_ticks:
        ticks = np.linspace(min_tick_val or min(ticks), max_tick_val or max(ticks), max_ticks)

    ticks = ticks.astype(dtype)

    getattr(ax, f"set_{which}ticks")(ticks)

    getattr(ax, f"set_{which}ticklabels")(ticks,
                                          weight=weight,
                                          fontsize=fontsize,
                                          **kwargs
                                          )
    return ax

# %%

def cross_val_score(
        model,
        x,
        y,
        scoring: Union [str, List[str], Callable] = 'r2',
        cross_validator = "KFold",
        cross_validator_args = None,
        process_results:bool = False,
        verbosity=1,
) -> Tuple[List, List, List, List]:
    """
    computes cross validation score

    Parameters
    ----------
        x :
            input data
        y :
            output corresponding to ``x``.
        scoring : (default=None)
            performance metric to use for cross validation.
            If None, it will be taken from config['val_metric']
            If callable then it must be a function which can take true and predicted
            arrays and return a float.

        process_results : bool, optional
            whether to process results at each cv iteration or not

    Returns
    -------
    list
        cross validation score for each of metric in scoring

    Example
    -------
    >>> from ai4water.datasets import busan_beach
    >>> from ai4water import Model
    >>> model = Model(model="RandomForestRegressor",
    >>>               cross_validator={"KFold": {"n_splits": 5}})
    >>> model.cross_val_score(data=busan_beach())

    We can also have our own performance metric as scoring

    >>> from ai4water.datasets import MtropicsLaos
    >>> df = MtropicsLaos().make_classification(lookback_steps=1)
    >>> def f1_score_(t,p)->float:
    >>>    return ClassificationMetrics(t, p).f1_score(average="macro")
    >>> model = Model(model="RandomForestClassifier",
    ... cross_validator={"KFold": {"n_splits": 5}},)
    >>> model.cross_val_score(data=df, scoring=f1_score_)

    Note
    ----
        Currently not working for deep learning models.

    """

    if not isinstance(scoring, list):
        scoring = [scoring]

    scores = []
    predictions = []
    observations = []
    test_indices = []

    if cross_validator_args is None:
        cross_validator_args = {'n_splits': 5}

    if isinstance(x, pd.DataFrame):
        x = x.values
    if isinstance(y, (pd.Series, pd.DataFrame)):
        y = y.values.reshape(-1,)

    if cross_validator == "KFold":
        kf = KFold(n_splits=cross_validator_args['n_splits'],
                   shuffle=True,
                   random_state=313)
        spliter = kf.split(x)
    else:
        kf = GroupKFold(n_splits=cross_validator_args['n_splits'])
        spliter = kf.split(x, groups=cross_validator_args['groups'])

    for fold, (tr_idx, test_idx) in enumerate(spliter):
        train_x = x[tr_idx]
        test_x = x[test_idx]
        train_y = y[tr_idx]
        test_y = y[test_idx]

        # make a new classifier/regressor at every fold
        model.build(model._get_dummy_input_shape())

        model.fit(x=train_x, y=train_y.reshape(-1, ))

        # since we have access to true y, it is better to provide it
        # it will be used for processing of results
        pred = model.predict(x=test_x, y=test_y, process_results=process_results)
        predictions.append(pred)
        observations.append(test_y)
        test_indices.append(test_idx)

        metrics = RegressionMetrics(test_y.reshape(-1, 1), pred)

        val_scores = []
        for score in scoring:
            if callable(score):
                val_scores.append(score(test_y.reshape(-1, 1), pred))
            else:
                val_scores.append(getattr(metrics, score)())

        scores.append(val_scores)

        if verbosity > 0:
            print(f'fold: {fold} val_score: {val_scores}')

    return scores, observations, predictions, test_indices



def fill_val(metric_name, default="min", default_min=99999999):
    if METRIC_TYPES.get(metric_name, default) == "min":
        return default_min
    return 0.0


def regression_plot(
        true,
        prediction,
        show:bool = True,
        hist_bins = 20,
        label="Removal Efficiency %",
):

    hist_kws =  {"linewidth":0.5, "edgecolor":"k",
                 'bins': hist_bins}

    scatter_kws = {'marker': "o", 'edgecolors': 'black',
                   'linewidth':0.8, 'alpha': 0.6}

    ax = regplot(true,
                 prediction,
                 fill_color="orange",
                 line_color='dimgray',
                 scatter_kws=scatter_kws,
                 marginals=True, show=False,
                 hist_kws=hist_kws,
                 ax_kws=dict(xlabel=f"Experimental {label}",
                             ylabel=f'Predicted {label}'))

    metrics = RegressionMetrics(true, prediction)

    r2 = metrics.r2()
    rmse = metrics.rmse()
    mae = metrics.mae()

    ax.annotate(f'$R^2$= {round(r2, 3)}',
                xy=(0.95, 0.35),
                xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='top',
                fontsize=12, weight="bold")
    ax.annotate(f'RMSE= {round(rmse, 3)}',
                xy=(0.95, 0.25),
                xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='top',
                fontsize=12, weight="bold")

    ax.annotate(f'MAE= {round(mae, 3)}',
                xy=(0.95, 0.15),
                xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='top',
                fontsize=12, weight="bold")

    if SAVE:
        label = label.replace('/', '_')
        plt.savefig(f"results/figures/reg_{label}.png",
                    bbox_inches="tight",
                    dpi=600)

    if show:
        plt.show()
    return ax

# %%

def print_version_info():
    info = get_version_info()

    for k,v in info.items():
        print(k, v)
    return
